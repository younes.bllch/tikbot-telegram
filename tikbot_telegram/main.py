import logging
import os
import re

import sentry_sdk
from dotenv import load_dotenv, find_dotenv
from telegram import (
    MessageEntity,
)
from telegram.ext import (
    Updater,
    MessageHandler,
    Filters,
    CommandHandler,
    InlineQueryHandler,
)

from tikbot_telegram.handlers.inline import inline_handler
from tikbot_telegram.handlers.message_handler import tiktok_handler
from tikbot_telegram.handlers.start import start_handler

load_dotenv(find_dotenv())

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger("TikBot")

updater = Updater(token=os.getenv("TELEGRAM_TOKEN"), use_context=True)
dispatcher = updater.dispatcher

if os.getenv("SENTRY_DSN"):
    sentry_sdk.init(dsn=os.getenv("SENTRY_DSN"), traces_sample_rate=0.2)

link_regex = re.compile(r"^https:\/\/(www|m|vm)\.tiktok\.com\/.+$")

os.environ.setdefault("REDIS_HOST", "localhost")
os.environ.setdefault("REDIS_PORT", "6379")
os.environ.setdefault("REDIS_DB", "0")

try:
    assert "GOOGLE_APPLICATION_CREDENTIALS" in os.environ.keys()
    bucket_name = os.environ["BUCKET_NAME"]
except (AssertionError, KeyError):
    logger.critical(
        "Please set the GOOGLE_APPLICATION_CREDENTIALS and BUCKET_NAME environment variable"
    )
    exit(1)
try:
    tikbot_api_endpoint = os.environ["TIKBOT_API_ENDPOINT"]
except KeyError:
    logger.critical("Please set the TIKBOT_API_ENDPOINT environment variable")
    exit(1)


if __name__ == "__main__":
    handler = MessageHandler(
        (Filters.entity(MessageEntity.URL) | Filters.entity(MessageEntity.TEXT_LINK)),
        tiktok_handler,
    )
    dispatcher.add_handler(handler)
    dispatcher.add_handler(CommandHandler("start", start_handler))
    dispatcher.add_handler(InlineQueryHandler(inline_handler))
    logger.info("TikBot booted")
    updater.start_polling()
    updater.idle()
