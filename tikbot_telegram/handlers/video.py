import json
import shutil
from tempfile import NamedTemporaryFile
from urllib.parse import urljoin

import requests
from jsonpath_ng import parse
from sentry_sdk import add_breadcrumb, start_transaction
from telegram import ChatAction, ParseMode
from telegram.error import BadRequest

from tikbot_telegram.util import build_caption


def process_video(update, url: str, text: str, redis_instance):
    from tikbot_telegram.main import tikbot_api_endpoint, logger

    message = update.effective_message
    add_breadcrumb(
        category="telegram",
        message="Processing video %s, requested by %s" % (url, message.from_user.name),
        level="info",
    )

    video_data_res = requests.post(
        urljoin(tikbot_api_endpoint, "/api/v1/video/"), {"video": url}
    )

    if not video_data_res.ok:
        logger.error(
            "Failed to request from TikBot API: %s" % video_data_res.status_code
        )
        add_breadcrumb(
            category="tikbot",
            message="Failed to request from TikBot API: %s, %s"
            % (video_data_res.status_code, video_data_res.text),
            level="error",
        )
        message.reply_html(
            f"Could not download video \U0001f613, are you sure this is a TikTok <b>video</b>? \U0001f97a"
        )
        video_data_res.raise_for_status()
        return

    item_infos = video_data_res.json()

    fetch_key = lambda key: parse("$..%s" % key).find(item_infos)[0].value

    file_id = redis_instance.get(fetch_key("id"))
    if file_id:
        with start_transaction(op="cache", description="Return cached video"):
            logger.info("Returning cached video for %s" % url)
            try:
                reply = message.reply_video(
                    video=file_id.decode("utf-8"),
                    disable_notification=True,
                    caption=build_caption(item_infos, url, text, message),
                    parse_mode=ParseMode.HTML,
                )

                try:
                    message.delete()
                except BadRequest:
                    pass

                return reply
            except Exception as e:
                logger.warning("Failed to deliver from cache: %s" % e)
                redis_instance.delete(file_id)
    # Initialize a temporary file in-memory for storing and then uploading the video
    with start_transaction(op="download", description="Download and send video"):
        with NamedTemporaryFile(suffix=".mp4") as f:
            message.chat.send_action(action=ChatAction.UPLOAD_VIDEO)
            # Get the video URL
            video_url = fetch_key("videoUrl")
            if len(video_url) == 0:
                logger.error(
                    "Failed to find videoUrl in video meta: %s" % json.dumps(item_infos)
                )
                add_breadcrumb(
                    category="tiktok",
                    message="Failed to find videoUrl in video meta",
                    level="error",
                )
                message.reply_html(
                    f"Could not download video \U0001f613, TikTok gave a bad video meta response \U0001f97a"
                )
                raise
            with requests.get(
                video_url, stream=True, headers=item_infos.get("headers", {})
            ) as r:
                if not r.ok:
                    logger.debug(f"Failed to download video {item_infos}")
                    add_breadcrumb(
                        category="tiktok",
                        message="Failed to download video from TikTok: %s"
                        % r.status_code,
                        level="error",
                    )
                    message.reply_html(
                        f"Could not download video \U0001f613, TikTok gave a bad response \U0001f97a ({r.status_code})"
                    )
                    return
                shutil.copyfileobj(r.raw, f)

            logger.info("Processed video %s" % url)

            reply = message.reply_video(
                video=open(f.name, "rb"),
                disable_notification=True,
                caption=build_caption(item_infos, url, text, message),
                parse_mode=ParseMode.HTML,
            )

            try:
                message.delete()
            except BadRequest:
                pass

            redis_instance.set(fetch_key("id"), reply.video.file_id)

            return reply
