from urllib.parse import urljoin

import requests
import validators
from jsonpath_ng import parse
from sentry_sdk import capture_exception
from telegram import ParseMode, InlineQueryResultVideo

from tikbot_telegram.util import build_caption, upload_tiktok


def inline_handler(update, context):
    from tikbot_telegram.main import tikbot_api_endpoint, logger

    query = update.inline_query.query.split(" ")[0]
    logger.info("Received query: %s " % query)
    if validators.url(query):
        try:

            video_data_res = requests.post(
                urljoin(tikbot_api_endpoint, "/api/v1/video/"), {"video": query}
            )
            if not video_data_res.ok:
                return
            data = video_data_res.json()
            video_url = upload_tiktok(data)

            logger.info(
                "Processed inline query for %s, video URL: %s" % (query, video_url)
            )

            fetch_key = lambda key: parse("$..%s" % key).find(data)[0].value

            caption = build_caption(data, query, "")

            results = [
                InlineQueryResultVideo(
                    id=fetch_key("id"),
                    video_url=video_url,
                    mime_type="video/mp4",
                    width=fetch_key("width"),
                    height=fetch_key("height"),
                    caption=caption,
                    title="Send this video",
                    description=fetch_key("text"),
                    thumb_url=fetch_key("default"),
                    parse_mode=ParseMode.HTML,
                )
            ]
            update.inline_query.answer(results)
        except Exception as e:
            capture_exception(e)
            logger.info("Failed to process inline query %s: %s" % (query, e))
            return
