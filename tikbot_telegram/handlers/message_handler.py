import os

from redis import Redis
from sentry_sdk import configure_scope, capture_exception, start_transaction
from telegram import MessageEntity

from tikbot_telegram.handlers.video import process_video


def tiktok_handler(update, context):
    from tikbot_telegram.main import link_regex, logger

    with configure_scope() as scope:
        message = update.effective_message
        scope.set_user(
            {
                "username": message.from_user.username,
                "id": message.from_user.link,
            }
        )
        scope.set_extra("message", message.text)
        message_entities = [
            n
            for n in message.parse_entities([MessageEntity.URL]).values()
            if link_regex.match(n)
        ]
        # The input message without the TikTok URL
        original_message = "".join(
            [str(message.text).replace(url, "") for url in message_entities]
        )
        # Iterate over all TikTok URLs
        redis_instance = Redis(
            host=os.environ.get("REDIS_HOST"),
            port=int(os.environ.get("REDIS_PORT")),
            db=int(os.environ.get("REDIS_DB")),
        )

        for url in message_entities:
            try:
                with start_transaction(op="task", name=url):
                    process_video(update, url, original_message, redis_instance)
            except Exception as e:
                capture_exception(e)
                logger.warning("Failed to download video %s: %s" % (url, repr(e)))
