import html
import shutil
from tempfile import NamedTemporaryFile

import requests
from emoji import emojize
from google.cloud import storage
from jsonpath_ng import parse
from telegram import Chat


def truncate(s, x=3000):
    return s[:x] + (s[x:] and "..")


def build_caption(item_infos, url, text, message=None):
    fetch_key = lambda key: parse("$..%s" % key).find(item_infos)[0].value

    author = "@%s (%s) %s" % (
        fetch_key("name"),
        fetch_key("nickName"),
        emojize(":white_check_mark:", use_aliases=True)
        if fetch_key("verified") is True
        else emojize(":lock:", use_aliases=True)
        if fetch_key("private")
        else "",
    )
    video_caption = fetch_key("text")
    if "#" in video_caption:
        video_caption = video_caption.split("#")[0]
    likes = fetch_key("diggCount") or 0
    comments = fetch_key("commentCount") or 0
    plays = fetch_key("playCount") or 0
    video_caption = html.escape(video_caption.strip())

    for mention in fetch_key("mentions"):
        video_caption = video_caption.replace(
            mention, f"<a href='https://tiktok.com/{mention}'>{mention}</a>"
        )

    user_part = ""
    if message is not None:
        if message.chat.type is not Chat.PRIVATE and message.from_user is not None:
            user_part = (
                f"{message.from_user.name} "
                if not text
                else f"{message.from_user.name}: {text}\n "
            )

    return (
        user_part
        + "\n%s" % author
        + (
            f'\n<a href="{url}">{video_caption}</a>\n'
            if len(video_caption.replace(" ", "")) > 0
            else f'\n<a href="{url}">TikTok</a>\n'
        )
        + (
            f"{int(likes):,} \u2764\ufe0f {int(comments):,} \U0001f4ad {int(plays):,} \u23ef"
        )
    )


def upload_tiktok(video_meta):
    fetch_key = lambda key: parse("$..%s" % key).find(video_meta)[0].value
    from tikbot_telegram.main import bucket_name

    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)

    blob = bucket.blob("%s.mp4" % fetch_key("id"))

    with NamedTemporaryFile(suffix=".mp4") as f:
        with requests.get(
            fetch_key("videoUrl"),
            stream=True,
            headers=video_meta.get("headers", {}),
        ) as r:
            r.raise_for_status()
            shutil.copyfileobj(r.raw, f)
            f.seek(0)
            blob.upload_from_file(f)

    blob.make_public()
    return blob.media_link
