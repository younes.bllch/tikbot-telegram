# TikBot

![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/tikbot/tikbot-telegram)

See TikTok videos in your Telegram chats without hassle!
This bot will upload all posted TikTok links as a video with stats and caption.

### [**Add this bot to your group chat or text it now!**](https://t.me/tiktokurlbot)

## Technical setup

- This is the telegram component for the [TikBot project](https://gitlab.com/tikbot/).
- There is an [API repository](https://gitlab.com/tikbot/tikbot-api/) which exposes the neccesary backend for fetching the video meta information
- Dependency management is done via [poetry](https://python-poetry.org/) (for development setup, see [CONTRIBUTING.md](CONTRIBUTING.md))

## Deployment

If you want to deploy your own instance of TikBot follow these instructions:

- Firstly you need to generate a new bot token by following [Telegrams instructions](https://core.telegram.org/bots#3-how-do-i-create-a-bot).
    - Enable group chat mode and inline mode
- This application has a [meta repository](https://gitlab.com/tikbot/tikbot), that contains a `docker-compose.yml` deployment file.
    - The two repositories have their own [GitLab CI configurations](.gitlab-ci.yml), which publishes the respective Docker images ([telegram](https://gitlab.com/tikbot/tikbot-telegram/container_registry), [api](https://gitlab.com/tikbot/tikbot-api/container_registry)).
    - The application requires a [redis](https://redis.io/) server.
- For secrets handling and required environment variables, refer to the [CONTRIBUTING.md](CONTRIBUTING.md) document.
